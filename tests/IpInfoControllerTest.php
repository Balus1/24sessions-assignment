<?php

use Silex\WebTestCase;

class IpInfoControllerTest extends WebTestCase {

    public function createApplication() {
        $app = new Silex\Application();
        $app->register(new Silex\Provider\TwigServiceProvider(), array(
            'twig.path' => __DIR__ . '/../views',
        ));
        ActiveRecord\Config::initialize(function($cfg) {
            $cfg->set_model_directory('Models');
            $cfg->set_connections([
                'test' => 'mysql://test_user:secret@localhost/test',
            ]);
        });
        require_once __DIR__ . '/../Controllers/IpinfoController.php';
        $app->mount('/', new IpinfoController());
        $app->post('/ipinfo', 'IpinfoController::ipinfo');
        $app->get('/', 'IpinfoController::index');
        return $app;
    }

    public function testIndexLoadsTemplate() {
        $client = $this->createClient();
        $client->request('GET', '/');
        $this->assertTrue($client->getResponse()->isOk());
        $this->assertContains('<body', $client->getResponse()->getContent());
    }

    public function testIpinfoGetsProperResponse() {
        $client = $this->createClient();
        $client->request('POST', '/ipinfo');
        $this->assertTrue($client->getResponse()->isOk());
    }

    public function testIpinfoSavesToDB() {
        $client = $this->createClient();
        $before = Ipinfo::count();
        $client->request('POST', '/ipinfo');
        $after = Ipinfo::count();
        $this->assertGreaterThan($before, $after);
    }

    public function testIpinfoGivesProperResponse() {
        $client = $this->createClient();
        $client->request('POST', '/ipinfo');
        $this->assertEquals($client->getResponse()->getStatusCode(), 200);
        $response_content = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('country', $response_content);
        $this->assertNotNull($response_content['country']);
        $this->assertArrayHasKey('city', $response_content);
        $this->assertNotNull($response_content['city']);
    }

}