<?php

ini_set('display_errors', 1);

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/Controllers/IpinfoController.php';

$app = new \Silex\Application();
$app['debug'] = true;
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/views',
));

ActiveRecord\Config::initialize(function($cfg) {
    $cfg->set_model_directory('Models');
    $cfg->set_connections([
        'development' => 'mysql://test_user:secret@localhost/test',
    ]);
});

// Routes.
$app->mount('/', new IpinfoController());
$app->post('/ipinfo', 'IpinfoController::ipinfo');
$app->get('/', 'IpinfoController::index');

$app->run();