<?php

use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class IpinfoController implements ControllerProviderInterface {

    public function connect(Application $app) {
        return $app['controllers_factory'];
    }

    public function index(Request $request, Application $app) {
        return $app['twig']->render('index.twig', ['ip' => $request->getClientIp()]);
    }

    public function ipinfo(Request $request, Application $app) {
        try {
            // Could have been done by CURL but that looks elegant and avoids settings issues.
            $info = file_get_contents('https://ipinfo.io/' . $request->getClientIp());
            if ($info === false) throw new Exception('Invalid API response.');
            $info = json_decode($info);
            if (!isset($info->city) || !isset($info->country)) throw new Exception('Invalid API response.');
            $ipinfo = new Ipinfo();
            $ipinfo->city = $info->city;
            $ipinfo->country = $info->country;
            $ipinfo->save();
            if ($ipinfo->id == null) throw new Exception('The object was not saved in the DB.');
        } catch (Exception $e) {
            return new Response($e->getMessage(), 500);
        }
        return new Response($ipinfo->to_json(), 200);
    }

}