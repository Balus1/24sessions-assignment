var Ipinfo = {};
Ipinfo.$button = null;
Ipinfo.$country = null;
Ipinfo.$city = null;
Ipinfo.getGeolocation = function() {
    $.post(window.location + 'ipinfo', {}, function(response) {
        response = JSON.parse(response);
        Ipinfo.$country.html(response.country);
        Ipinfo.$city.html(response.city);
    });
};
Ipinfo.init = function () {
    Ipinfo.$button = $('#button');
    Ipinfo.$button.click(Ipinfo.getGeolocation);
    Ipinfo.$country = $('#country');
    Ipinfo.$city = $('#city');
};

$(function() {
    Ipinfo.init();
});